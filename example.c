#include <stdio.h>
#include "aiwendb.h"


int main() {
    // awdb 文件路径
    char *filename = "test.awdb";
    AWDB_s awdb;
    AWDB_file_open(filename, &awdb); 
    // 查询IP地址
    char *ip_address = "211.112.21.0";
    // printf("待查询IP %s\n", ip_address);
    // char *ip_address = "2001:023a:0000:0000:0000:0000:0000:0000";
    AIWEN_result result = AWDB_reader_find(&awdb, ip_address);
    // 判断是否查询到数据
    if (result.is_found == true) {
        // printf("awdb.file_size %d\n", awdb.file_size);
        // printf("result.entry.offset %d\n", result.entry.offset);
        if (awdb.file_size < result.entry.offset) {
            printf("Invalid node_index in search tree\n");
            return 0;
        }
	    // 输出查找结果
        AWDB_resolve(stdout,&awdb, &result);
    } else {
        printf("没有相关数据\n");
    }
    AWDB_close(&awdb);
}


