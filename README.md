# 部署过程
## 1、依赖安装
- Ubuntu：
  ```shell
    $ sudo apt-get  install -y  dh-autoreconf  make
  ```
- centos：
  ```shell
     $ yum install –y gcc make autoconf automake  libtool libsysfs
  ```
## 2、gcc安装步骤（gcc 版本需要 5.4+）
- wget https://ftp.gnu.org/gnu/gcc/gcc-5.4.0/gcc-5.4.0.tar.bz2
- tar -jxvf gcc-5.4.0.tar.bz2
- cd gcc-5.4.0
- ./contrib/download_prerequisites
- mkdir gcc-build-5.4.0
- cd gcc-build-5.4.0
- ../configure --enable-checking=release --enable-languages=c,c++ --disable-multilib
- make
- make install
- cd /usr/bin/
- mv gcc gcc_back
- mv g++ g++_back
- ln -s /usr/local/bin/gcc gcc
- ln -s /usr/local/bin/g++ g++

## 3、解析程序安装(安装一次即可):
- sh bootstrap
- ./configure --disable-tests
- sudo make
- sudo make install
- sudo sh -c "echo /usr/local/lib  >> /etc/ld.so.conf.d/local.conf"
- sudo ldconfig

## 4、程序编译
- gcc example.c -I./include -laiwendb -o awdb  执行后生成 awdb 可执行程序
- ./awdb
